#! /usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt


def dist(x, w):
    return np.linalg.norm(x - w)


def argmin(x, W):
    d = np.linalg.norm(x-W[0,0])
    r = 0
    c = 0
    for i in range(np.shape(W)[0]):
        for j in range(np.shape(W)[1]):
            di = np.linalg.norm(x-W[i,j])
            if di < d:
                d = di
                r = i
                c = j

    return r, c, d


def gauss_neighbor(dij, sig):
    return np.exp(- np.power(dij,2) / (2 * np.power(sig, 2)))


class SOM:

    def __init__(self, row, col, W0):
        self.row = row
        self.col = col
        self.W = W0

    def train(self, X, max_epoch=100, lrate=0.01, is_plot=False):
        sig0 = 5
        sig = sig0
        T = 10

        for epoch in range(max_epoch):
            if is_plot:
                plt.cla()
                plt.plot(X[:, 0], X[:, 1], 'bo')
                self.plot()
                plt.suptitle('Epoch ' + str(epoch))
                plt.pause(0.1)

            total_error = 0
            for id in range(np.shape(X)[0]):
                x = X[id,:]
                r, c, _ = argmin(x, self.W)

                for ri in range(self.row):
                    for ci in range(self.col):
                        dij = np.power(r - ri, 2) + np.power(c - ci, 2)
                        lij = np.exp(- dij / (2 * np.power(sig, 2)))
                        self.W[ri,ci] = self.W[ri, ci] + lrate * lij * (x - self.W[ri, ci])

                total_error = total_error + np.linalg.norm(x - self.W[r, c])
                sig = sig0 * np.exp(- epoch / T)

            avg_error = total_error / np.shape(X)[0]
            print('avg_error: {}'.format(avg_error))
            if avg_error < 1e-3:
                break

    def plot(self):
        for ri in range(self.row):
            for ci in range(self.col):
                plt.plot(self.W[ri,ci,0], self.W[ri,ci,1], 'ro')

                if ci < self.col - 1:
                    plt.plot([self.W[ri,ci,0], self.W[ri, ci+1,0]], [self.W[ri,ci,1], self.W[ri, ci+1,1]], 'k-')

                if ri < self.row - 1:
                    plt.plot([self.W[ri,ci,0], self.W[ri+1, ci,0]], [self.W[ri,ci,1], self.W[ri+1, ci,1]], 'k-')



if __name__ == "__main__":

    X = np.zeros(shape=(100, 2))
    X[0:30, :] = np.random.multivariate_normal(mean=[0, 0], cov=np.array([[0.5,0], [0,0.5]]), size=30)
    X[30:60, :] = np.random.multivariate_normal(mean=[10, 10], cov=np.array([[0.5,0], [0,0.5]]), size=30)
    X[60:, :] = np.random.multivariate_normal(mean=[0, 10], cov=np.array([[1,0], [0,1]]), size=40)

    row = 10
    col = 10

    W0 = np.zeros(shape=(row, col, 2))
    for ri in range(row):
        for ci in range(col):
            W0[ri,ci] = np.array([ri,ci])

    som = SOM(row, col, W0=W0)
    som.train(X, 100, is_plot=True)


