#! /usr/bin/env python3

import os
import numpy as np
import matplotlib.pyplot as plt

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import tensorflow as tf
from tensorflow import keras


def plot_grid(decoder, grid_size=10, low=-5.0, high=5.0):
    grid = np.zeros((28 * grid_size, 28 * grid_size))
    grid_x = np.linspace(low, high, grid_size)
    grid_y = np.linspace(low, high, grid_size)

    for i in range(10):
        yi = grid_y[i]
        for j in range(10):
            xi = grid_x[j]
            z_latent = np.array([[xi, yi]])
            x_ = decoder(z_latent)
            grid[i * 28 : (i + 1) * 28, j * 28 : (j + 1) * 28] = tf.reshape(x_[0], (28, 28))

    plt.figure()
    end_range = 28 * grid_size + 15
    labels_pos = np.arange(14, end_range, 28)
    label_x = np.round(grid_x, 1)
    label_y = np.round(grid_y, 1)
    plt.xticks(labels_pos[:-1], label_x)
    plt.yticks(labels_pos[:-1], label_y)
    plt.xlabel("latent dimension x")
    plt.ylabel("latent dimension y")
    plt.imshow(grid, cmap="Greys_r")
    plt.show()


def plot_latent(ax, x, z, size=[0.2,0.2]):
    z_min = tf.math.reduce_min(z_show, axis=0)
    z_max = tf.math.reduce_max(z_show, axis=0)
    ax.set_xlim(z_min[0]-1, z_max[0]+1)
    ax.set_ylim(z_min[1]-1, z_max[1]+1)
    for i in range(np.shape(x)[0]):
        left = z[i,0] - size[0]
        right = z[i,0] + size[0]
        top = z[i,1] + size[1]
        bottom = z[i,1] - size[1]
        cx = x[i,:]
        cx = tf.reshape(cx, (28, 28))
        ax.imshow(cx, extent=(left, right, bottom, top), cmap="Greys_r")


class MultiLayersPerceptron(keras.layers.Layer):

    def __init__(self, layers, name="MultiLayersPerceptron", **kwargs):
        super(MultiLayersPerceptron, self).__init__(name=name, **kwargs)
        self.layers = []
        for i in range(len(layers)):
            self.layers.append(tf.keras.layers.Dense(layers[i], activation="relu"))

    def call(self, inputs):
        outputs = inputs
        for i in range(len(self.layers)):
            outputs = self.layers[i](outputs)

        return outputs


class AutoEncoder(keras.Model):

    def __init__(self, encoder_layers, decoder_layers, name="AutoEncoder", **kwargs):
        super(AutoEncoder, self).__init__(name=name, **kwargs)
        self.encoder = MultiLayersPerceptron(layers=encoder_layers, name="encoder")
        self.decoder = MultiLayersPerceptron(layers=decoder_layers, name="decoder")
        self.mse_tracker = keras.metrics.Mean(name="mse")

    def decode(self, latent_inputs):
        return self.decoder(latent_inputs)

    def encode(self, inputs):
        return self.encoder(inputs)

    def call(self, inputs):
        return self.decoder(self.encoder(inputs))

    def train_step(self, data):
        x, _ = data
        with tf.GradientTape(persistent=True) as tape:
            z = self.encoder(x)
            x_ = self.decoder(z)
            mse = tf.reduce_mean(tf.norm(x - x_, axis=1))

        grad = tape.gradient(mse, self.trainable_variables)
        self.optimizer.apply_gradients(zip(grad, self.trainable_variables))
        self.mse_tracker.update_state(mse)
        return {"mse": self.mse_tracker.result()}


if __name__ == "__main__":
    from tensorflow.keras.datasets import mnist
    (x_train_, _), (x_test_, _) = mnist.load_data()
    x_train = x_train_.astype('float32') / 255.
    x_test = x_test_.astype('float32') / 255.
    x_train = x_train.reshape((len(x_train), np.prod(x_train.shape[1:])))
    x_test = x_test.reshape((len(x_test), np.prod(x_test.shape[1:])))

    encoder_layers = [256, 64, 2]
    decoder_layers = [64, 256, 784]
    ae = AutoEncoder(encoder_layers=encoder_layers, decoder_layers=decoder_layers)
    ae.compile(keras.optimizers.Adam())
    ae.fit(x_train, x_train, verbose=2, batch_size=200, epochs=500)

    x_rec = ae(x_test)
    mse = tf.reduce_mean(tf.norm(x_test - x_rec, axis=1))
    print('test reconstruction error: {}'.format(mse))

    fig, axes = plt.subplots(2, 5)

    for i in range(5):
        axes[0,i].imshow(x_test_[i, :, :])
        x_ = x_rec[i, :] * 255.
        x_ = tf.reshape(x_, (28, 28))
        axes[1,i].imshow(x_)

    fig1, ax = plt.subplots()

    x_show = x_train[:100,:]
    z_show = ae.encode(x_show)
    plot_latent(ax, x_show, z_show, size=[0.5, 0.5])
    plot_grid(ae.decoder, 10, 0, 30)