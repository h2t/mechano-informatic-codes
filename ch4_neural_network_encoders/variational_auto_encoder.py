#! /usr/bin/env python3

import os

import numpy as np
import matplotlib.pyplot as plt

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import tensorflow as tf
from tensorflow import keras


def plot_grid(z_mean, decoder, grid_size=10, low=-5.0, high=5.0):
    grid = np.zeros((28 * grid_size, 28 * grid_size))
    grid_x = np.linspace(low, high, grid_size)
    grid_y = np.linspace(low, high, grid_size)

    for i in range(grid_size):
        yi = grid_y[i]
        for j in range(grid_size):
            xi = grid_x[j]
            z_latent = z_mean + np.array([[xi, yi]])
            x_ = decoder(z_latent)
            grid[i * 28 : (i + 1) * 28, j * 28 : (j + 1) * 28] = tf.reshape(x_[0], (28, 28))

    plt.figure()
    end_range = 28 * grid_size + 15
    labels_pos = np.arange(14, end_range, 28)
    label_x = np.round(grid_x, 1)
    label_y = np.round(grid_y, 1)
    plt.xticks(labels_pos[:-1], label_x)
    plt.yticks(labels_pos[:-1], label_y)
    plt.xlabel("latent dimension x")
    plt.ylabel("latent dimension y")
    plt.imshow(grid, cmap="Greys_r")
    plt.show()


def plot_latent(ax, x, z, size=[0.2,0.2]):
    z_min = tf.math.reduce_min(z, axis=0)
    z_max = tf.math.reduce_max(z, axis=0)
    ax.set_xlim(z_min[0]-1, z_max[0]+1)
    ax.set_ylim(z_min[1]-1, z_max[1]+1)
    for i in range(np.shape(x)[0]):
        left = z[i,0] - size[0]
        right = z[i,0] + size[0]
        top = z[i,1] + size[1]
        bottom = z[i,1] - size[1]
        cx = x[i,:]
        cx = tf.reshape(cx, (28, 28))
        ax.imshow(cx, extent=(left, right, bottom, top), cmap="Greys_r")


class VAEncoder(keras.layers.Layer):

    def __init__(self, feat_layers, z_mean_layers, z_log_var_layers, use_conv=False, name="VAEncoder", **kwargs):
        super(VAEncoder, self).__init__(name=name, **kwargs)
        self.feat_layers = []
        for i in range(len(feat_layers)):
            self.feat_layers.append(tf.keras.layers.Dense(feat_layers[i], activation="relu"))

        self.z_mean_layers = []
        for i in range(len(z_mean_layers)):
            self.z_mean_layers.append(tf.keras.layers.Dense(z_mean_layers[i], activation="relu"))

        self.z_log_var_layers = []
        for i in range(len(z_log_var_layers)):
            self.z_log_var_layers.append(tf.keras.layers.Dense(z_log_var_layers[i], activation="relu"))

        self.use_conv = use_conv
        self.conv_layers = []
        if self.use_conv:
            self.conv_layers.append(tf.keras.layers.Conv2D(32, 3, activation="relu", strides=2, padding="same"))
            self.conv_layers.append(tf.keras.layers.Conv2D(64, 3, activation="relu", strides=2, padding="same"))
            self.conv_layers.append(tf.keras.layers.Flatten())

    def call(self, inputs):
        if self.use_conv:
            inputs = tf.reshape(inputs, (-1, 28, 28))
            inputs = tf.expand_dims(inputs, axis=-1)

        feat = inputs
        for i in range(len(self.conv_layers)):
            feat = self.conv_layers[i](feat)

        for i in range(len(self.feat_layers)):
            feat = self.feat_layers[i](feat)

        z_mean = feat
        for i in range(len(self.z_mean_layers)):
            z_mean = self.z_mean_layers[i](z_mean)

        z_log_var = feat
        for i in range(len(self.z_log_var_layers)):
            z_log_var = self.z_log_var_layers[i](z_log_var)

        return z_mean, z_log_var


class VADecoder(keras.layers.Layer):

    def __init__(self, layers, use_conv=False, name="VADecoder", **kwargs):
        super(VADecoder, self).__init__(name=name, **kwargs)
        self.layers = []
        for i in range(len(layers)):
            self.layers.append(tf.keras.layers.Dense(layers[i], activation="relu"))

        self.use_conv = use_conv
        self.conv_layers = []
        if self.use_conv:
            self.conv_layers.append(tf.keras.layers.Reshape((7, 7, 64)))
            self.conv_layers.append(tf.keras.layers.Conv2DTranspose(64, 3, activation="relu", strides=2, padding="same"))
            self.conv_layers.append(tf.keras.layers.Conv2DTranspose(32, 3, activation="relu", strides=2, padding="same"))
            self.conv_layers.append(tf.keras.layers.Conv2DTranspose(1, 3, activation="relu", padding="same"))


    def call(self, inputs):
        outputs = inputs
        for i in range(len(self.layers)):
            outputs = self.layers[i](outputs)

        for i in range(len(self.conv_layers)):
            outputs = self.conv_layers[i](outputs)

        if self.use_conv:
            outputs = tf.reshape(outputs, (-1, 784))

        return outputs


class VariationalAutoEncoder(keras.Model):

    def __init__(self, feat_layers, z_mean_layers, z_log_var_layers, decoder_layers, use_conv=False, name="VariationalAutoEncoder", **kwargs):
        super(VariationalAutoEncoder, self).__init__(name=name, **kwargs)
        self.encoder = VAEncoder(feat_layers, z_mean_layers, z_log_var_layers, name="encoder", use_conv=use_conv)
        self.decoder = VADecoder(layers=decoder_layers, name="decoder", use_conv=use_conv)
        self.rec_loss_tracker = keras.metrics.Mean(name="rec_loss")
        self.kl_loss_tracker = keras.metrics.Mean(name="kl_loss")
        self.use_conv = use_conv

    def decode(self, latent_inputs):
        return self.decoder(latent_inputs)

    def encode(self, inputs):
        return self.encoder(inputs)

    def sample(self, z_mean, z_log_var):
        batch_size = tf.shape(z_mean)[0]
        dim = tf.shape(z_mean)[1]
        epsilon = tf.keras.backend.random_normal(shape=(batch_size, dim))
        samples = z_mean + tf.math.exp(0.5 * z_log_var) * epsilon
        return samples

    def get_kl_loss(self, z_mean, z_log_var):
        kl_divergence = 0.5 * (tf.math.square(z_mean) + tf.math.exp(z_log_var) - 1 - z_log_var)
        kl_divergence = tf.reduce_mean(kl_divergence)
        return kl_divergence

    def call(self, inputs):
        z_mean, z_log_var = self.encoder(inputs)
        samples = self.sample(z_mean, z_log_var)
        x_ = self.decoder(samples)
        return x_

    def train_step(self, data):
        x, _ = data
        with tf.GradientTape(persistent=True) as tape:
            z_mean, z_log_var = self.encoder(x)
            samples = self.sample(z_mean, z_log_var)
            x_ = self.decoder(samples)
            rec_loss = tf.reduce_mean(tf.norm(x - x_, axis=1))
            kl_loss = self.get_kl_loss(z_mean, z_log_var)
            loss = 100 * rec_loss + kl_loss

        grad = tape.gradient(loss, self.trainable_variables)
        self.optimizer.apply_gradients(zip(grad, self.trainable_variables))
        self.rec_loss_tracker.update_state(rec_loss)
        self.kl_loss_tracker.update_state(kl_loss)
        return {"rec_loss": self.rec_loss_tracker.result(), "kl_loss": self.kl_loss_tracker.result()}


if __name__ == "__main__":

    from tensorflow.keras.datasets import mnist
    (x_train_, _), (x_test_, _) = mnist.load_data()
    x_train = x_train_.astype('float32') / 255.
    x_test = x_test_.astype('float32') / 255.
    x_train = x_train.reshape((len(x_train), np.prod(x_train.shape[1:])))
    x_test = x_test.reshape((len(x_test), np.prod(x_test.shape[1:])))

    use_conv = True
    if use_conv:
        feat_layers = [64]
        z_mean_layers = [16, 2]
        z_log_var_layers = [16, 2]
        decoder_layers = [7 * 7 * 64]
    else:
        feat_layers = [256, 128]
        z_mean_layers = [64, 2]
        z_log_var_layers = [64, 2]
        decoder_layers = [64, 256, 784]

    vae = VariationalAutoEncoder(
        feat_layers=feat_layers, z_mean_layers=z_mean_layers, z_log_var_layers=z_log_var_layers,
        decoder_layers=decoder_layers, use_conv=use_conv)
    vae.compile(keras.optimizers.Adam())
    vae.fit(
        x_train, x_train, verbose=1, batch_size=500,
        # Try different number of epochs:
        # epochs=10,
        epochs=100,
    )

    x_rec = vae(x_test)
    mse = tf.reduce_mean(tf.norm(x_test - x_rec, axis=1))
    print('test reconstruction error: {}'.format(mse))

    fig, axes = plt.subplots(2, 5)

    for i in range(5):
        axes[0,i].imshow(x_test_[i, :, :])
        x_ = x_rec[i, :] * 255.
        x_ = tf.reshape(x_, (28, 28))
        axes[1,i].imshow(x_)

    fig1, ax = plt.subplots()

    z_mean, _ = vae.encode(x_train[:100,:])
    z_mean = tf.reduce_mean(z_mean, axis=0)
    x_show = x_train[:100,:]
    z_show, _ = vae.encode(x_show)
    plot_latent(ax, x_show, z_show, size=[0.5, 0.5])
    plot_grid(z_mean, vae.decoder, grid_size=20, low=-2.0, high=2.0)
