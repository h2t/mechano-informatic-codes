#! /bin/bash

dir=$1

ffmpeg -framerate 25 -i "$dir/round-%00d.png" -an -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p "$dir.mp4" -y
