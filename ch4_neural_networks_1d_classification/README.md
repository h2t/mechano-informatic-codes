# Neural Network 1D Classification 

These examples show simple 1D classification examples on clean, noisy and erroneous data 
as well as training with regularization.  


## Running the Examples

You can set up the virtual environment as with all other examples.

To run an example, activate the environment 
and just call the script in the command line:

```bash
cd path/to/ch4_neural_networks_1d_classification
source venv/bin/activate

python classify-1d-2cls-clean.py
python classify-1d-2cls-noise.py
python classify-1d-2cls-error.py
python classify-1d-2cls-error-regul.py
```

The examples generate images in the `plots/` directory. 
You can turn them into video animations using the `make_video.sh` script:

```bash
make_video.sh classify-1d-2cls-clean.py
make_video.sh classify-1d-2cls-noise.py
make_video.sh classify-1d-2cls-error.py
make_video.sh classify-1d-2cls-error-regul.py
```
