# Code Examples from the lecture Mechano Informatics by H2T 

## Setup a Virtual Environment

The examples are structured to be standalone, 
i.e. you can set up and run them independently of each other.

Each example provides a `requirement.txt`, which you can use to
set up a virtual environment for this example.
To do this, navigate to the example of interest and run these 
commands in a command line:

```bash
cd path/to/the/example

python3 -m venv venv
source venv/bin/activate

pip install --upgrade pip
pip install -r requirements.txt
```


## Run a Notebook

Most examples are implemented in a [Jupyter Notebook](https://jupyter.org/).
To run them, activate the example's virtual environment and run `jupyter notebook`:

```bash
cd path/to/the/example
source venv/bin/activate

jupyter notebook
```

A web-page should automatically be opened it your web browser.
If not, open the URL that is printed by the `jupyter notebook` command
(e.g. `file:///home/user/.local/share/jupyter/runtime/nbserver-22023-open.html`)

The notebook files end with a `.ipynb` extension. 
Open a notebook in the web interface.

You can now explore the notebook by 
running the cells one by one and editing the code to experiment with the example.
