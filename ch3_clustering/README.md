# Clustering

This interactive notebook shows how to use k-means and Gaussian Mixture Models (GMM) using scikit-learn.
We apply these algorithms on different datasets and visualize the results.
While both can be used for clustering, GMMs are generative models, thus we will also show how to sample new data from such models.

