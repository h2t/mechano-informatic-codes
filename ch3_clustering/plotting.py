import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
from matplotlib import colors
import seaborn as sns

sns.set()


def draw_ellipse(_mean, _covariance, _n_std=3, _ax=None, **kwargs):
    _ax = _ax or plt.gca()

    _u, _s, _vt = np.linalg.svd(_covariance)
    _angle = np.degrees(np.arctan2(_u[1, 0], _u[0, 0]))
    _width, _height = 2 * np.sqrt(_s)

    for _i in range(1, _n_std + 1):
        _ax.add_patch(Ellipse(_mean, _i * _width, _i * _height, _angle, **kwargs))


def plot_clustering(_data, _labels=None, _centers=None, _covars=None, _weights=None, _n_std=3,
                    _decision_boundary_model=None, _title=None, legend=False):
    if _labels is None:
        _labels = np.zeros(shape=(_data.shape[0],))

    _unique_labels = np.unique(_labels)
    _n_clusters = len(_unique_labels)
    _colors = sns.color_palette(n_colors=_n_clusters)
    _marker_colors = sns.color_palette("dark", n_colors=_n_clusters)

    _grid = sns.JointGrid(
        x=_data[:, 0],
        y=_data[:, 1],
        xlim=(0, 100),
        ylim=(0, 100),
        hue=_labels,
        palette=_colors
    )

    if _decision_boundary_model is not None:
        _cmap = colors.ListedColormap(_colors)
        _mesh_granularity = 0.1
        x_min, x_max = 0, 100
        y_min, y_max = 0, 100
        xx, yy = np.meshgrid(np.arange(x_min, x_max, _mesh_granularity), np.arange(y_min, y_max, _mesh_granularity))
        z = _decision_boundary_model.predict(np.c_[xx.ravel(), yy.ravel()])
        z = z.reshape(xx.shape)

        _grid.ax_joint.imshow(
            z,
            interpolation="nearest",
            extent=(xx.min(), xx.max(), yy.min(), yy.max()),
            cmap=_cmap,
            aspect="auto",
            origin="lower",
            alpha=0.3,
        )

    _grid.plot_joint(
        sns.scatterplot,
        style=_labels
    )

    _grid.plot_marginals(
        sns.kdeplot,
        fill=True,
        alpha=.5,
        warn_singular=False
    )

    if _centers is not None:
        sns.scatterplot(
            x=_centers[:, 0],
            y=_centers[:, 1],
            s=200,
            style=_unique_labels,
            hue=_unique_labels,
            palette=_marker_colors,
            ax=_grid.ax_joint,
        )

    if _covars is not None and _weights is not None:
        for _mean, _covar, _weight, _color in zip(_centers, _covars, _weights, _marker_colors):
            draw_ellipse(
                _mean, _covar,
                _n_std=_n_std,
                edgecolor='black',
                fill=False,
                linestyle="-",
                _ax=_grid.ax_joint
            )
            draw_ellipse(
                _mean, _covar,
                _n_std=_n_std,
                facecolor=_color,
                alpha=_weight * 0.2 / _weights.max(),
                _ax=_grid.ax_joint
            )

    if legend is False:
        _grid.ax_joint.get_legend().remove()
    else:
        legend_handles, legend_labels = _grid.ax_joint.get_legend_handles_labels()
        if len(legend_handles) == _n_clusters * 2:
            legend_handles = legend_handles[:_n_clusters]
            legend_labels = legend_labels[:_n_clusters]
        _grid.ax_joint.legend(legend_handles, legend_labels)

    if _title is not None:
        _grid.fig.suptitle(_title)
    _grid.fig.tight_layout()
    plt.show()


def plot_gmm_clustering(_data, _labels, _gmm, _n_std=3, _decision_boundary=False, _title=None):
    plot_clustering(
        _data, _labels,
        _centers=_gmm.means_,
        _covars=_gmm.covariances_,
        _weights=_gmm.weights_,
        _n_std=_n_std,
        _decision_boundary_model=_gmm if _decision_boundary else None,
        _title=_title
    )


def print_gmm(_gmm, _means=True, _covariances=True, _weights=False):
    print(f"GMM means:\n {_gmm.means_}\n") if _means else None
    print(f"GMM covariances:\n {_gmm.covariances_}\n") if _covariances else None
    print(f"GMM weights:\n {_gmm.weights_}\n") if _weights else None
