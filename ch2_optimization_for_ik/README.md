# Optimization for Inverse Kinematics

This example shows how optimization methods can be used
to solve the inverse kinematics (IK) problem of a 
single arm robot attached with a gripper.

> **Note:** `kuka.py` is based on the gym implementation for pybullet environment
([kuka.py in gym](https://github.com/bulletphysics/bullet3/blob/master/examples/pybullet/gym/pybullet_envs/bullet/kuka.py)).

