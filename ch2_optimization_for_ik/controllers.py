import os, inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
os.sys.path.insert(0, currentdir)

import numpy as np
import kuka

class Controller:
    def __init__(self, *kwargs):
        pass

    def set_target(self, target):
        pass

    def control(self, time, state):
        pass


class SimpleTCPPController(Controller):
    def __init__(self, kp):
        super().__init__()
        self.kp = kp
        self.set_target([0,0,0,0])
        self.to_ctrl = kuka.TCP

    def set_target(self, target):
        self.target = np.array(target)

    def control(self, time, state):
        """
        :param time:
        :param state: [x,y,z,a]
        :return:
        """
        obs = np.array(state)
        print(obs)
        dpos = self.kp * (self.target[:3] - obs[:3])
        dangle = self.kp * (np.mod(self.target[3], 2*np.pi) - np.mod(state[3], 2*np.pi))
        action = np.append(dpos, dangle)
        return action


class SimpleJointPController(Controller):
    def __init__(self, kp):
        super().__init__()
        self.kp = kp
        self.set_target([0.006418, 0.413184, -0.011401, -1.589317, 0.005379, 1.137684, 0])
        self.to_ctrl = kuka.JOINT

    def set_target(self, target):
        self.target = np.array(target)

    def control(self, time, state):
        """
        :param time:
        :param state: [q1,q2,q3,q4,q5,q6]
        :return:
        """
        obs = np.array(state)
        dpos = self.kp * (self.target - obs)
        dpos[-1] = 0
        return dpos


class SimpleTrajTracker(Controller):
    def __init__(self, kp):
        """
        :param kp: control parameter
        :param traj: N x 4 => N: timesteps, 4: dimension of each traj point
        if None: a dummy trajectory is used. It moves forward 1cm from the current point at each step
        """
        super().__init__()
        self.pCtrl = SimpleTCPPController(kp)
        self.set_target(None)
        self.to_ctrl = kuka.TCP

    def set_target(self, target_traj):
        self.traj = target_traj

    def control(self, time, state):
        """
        :param time: not the real time (order of the trajectory point)
        :param state: [x,y,z,a]
        :return:
        """

        if self.traj is None:
            target = np.array(state) + np.array([0, 0, 0.01, 0])
            self.pCtrl.set_target(target)
        else:
            self.pCtrl.set_target(self.traj[time,:])

        return self.pCtrl.control(time, state)


