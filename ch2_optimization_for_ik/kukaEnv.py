import os, inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
os.sys.path.insert(0, currentdir)

import pybullet as p
import pybullet_data
import kuka
import os
import time
from controllers import *
import numpy as np


class KukaGripperEnv:
    def __init__(self, urdfRoot=pybullet_data.getDataPath(), _render=True, _debug=True, block_pos=None):
        self._urdfRoot = urdfRoot
        self._timeStep = 1. / 240.
        self._render = _render
        self._debug = _debug
        self.timestep = 0
        self.to_ctrl = kuka.TCP
        if self._render:
            p.connect(p.GUI)
            self.curr_cam_dis = 1.3
            self.curr_cam_yaw = 180
            self.curr_cam_pitch = -41
            self.curr_cam_target = [0.52, -0.2, 0.0]
            p.resetDebugVisualizerCamera(1.3, 180, -41, [0.52, -0.2, 0.0])
            p.configureDebugVisualizer(p.COV_ENABLE_GUI, 0)
        else:
            p.connect(p.DIRECT)

        self.reset_env(block_pos)


    def reset_env(self, block_pos=None):
        if p.isConnected():
            p.resetSimulation()

        p.setPhysicsEngineParameter(numSolverIterations=150)
        p.setTimeStep(self._timeStep)
        p.loadURDF(os.path.join(self._urdfRoot, "plane.urdf"), [0, 0, -1])
        p.loadURDF(os.path.join(self._urdfRoot, "table/table.urdf"), 0.5000000, 0.00000, -.820000, 0.000000, 0.000000, 0.0, 1.0)

        if block_pos is not None:
            orn = p.getQuaternionFromEuler([0, 0, block_pos[-1]])
            self.blockUid = p.loadURDF(os.path.join(self._urdfRoot, "block.urdf"), block_pos[0], block_pos[1], block_pos[2], orn[0], orn[1], orn[2], orn[3])

        self._kuka = kuka.Kuka(urdfRootPath=self._urdfRoot, timeStep=self._timeStep)
        self.finger_joint = self._kuka.getFingerJoint()
        p.setGravity(0, 0, -10)
        self.timestep = 0
        return self._kuka.getObservation()

    def stopped(self):
        keys = p.getKeyboardEvents()

        is_stop = (ord('q') in keys)
        if is_stop:
            p.disconnect()

        return is_stop

    def started(self):
        keys = p.getKeyboardEvents()
        is_started = (p.B3G_SPACE in keys)
        return is_started


    def move_camera(self):
        keys = p.getKeyboardEvents()
        lkey = ord('a')
        rkey = ord('d')
        ukey = ord('w')
        dkey = ord('s')
        fkey = ord('f')
        ckey = ord('c')


        if lkey in keys:
            self.curr_cam_yaw += 1
        if rkey in keys:
            self.curr_cam_yaw -= 1
        if ukey in keys:
            self.curr_cam_pitch -= 0.1
        if dkey in keys:
            self.curr_cam_pitch += 0.1
        if fkey in keys:
            self.curr_cam_dis += 0.05
        if ckey in keys:
            self.curr_cam_dis -= 0.05


        reset_key = ord('r')
        if reset_key in keys:
            self.curr_cam_yaw = 180
            self.curr_cam_pitch = -41
            self.curr_cam_dis = 1.3
            self.curr_cam_target = [0.52, -0.2, 0.0]


        p.resetDebugVisualizerCamera(self.curr_cam_dis,
                                     self.curr_cam_yaw,
                                     self.curr_cam_pitch,
                                     self.curr_cam_target)

    def set_control_target(self, control_target=kuka.TCP):
        self.to_ctrl = control_target

    def step(self, action, _actionRepeat=1):
        if self.to_ctrl == kuka.TCP:
            return self.step_tcp(action, _actionRepeat)
        else:
            return self.step_joint(action, _actionRepeat)

    def step_tcp(self, action, _actionRepeat=1):
        """
        :param action: [dx, dy, dz, d_angle, finger_angle] or [dx, dy, dz, d_angle]
        :param _actionRepeat: integer => how many times will a specific action be called ?
        :return: kuka state: [x,y,z,rx,ry,rz]
        """
        if len(action) < 3 or len(action) > 6:
            raise ValueError('tcp action should have either 3, 4, 5 dimensions')

        if len(action) <= 4:
            if len(action) <= 3:
                action = np.append(action, 0)

            action = np.append(action, self.finger_joint)

        pos0 = self._kuka.getObservation()[:3]
        for i in range(_actionRepeat):
            self._kuka.applyAction(action, to_ctrl=kuka.TCP)
            p.stepSimulation()
            self.timestep += 1

        if self._debug:
            pos1 = self._kuka.getObservation()[:3]
            p.addUserDebugLine(pos0, pos1, lineWidth=30, lifeTime=20, lineColorRGB=[1,0,0])

        return self._kuka.getObservation()

    def step_joint(self, action, _actionRepeat=1):
        """
        :param action: [dq1,dq2,dq3,dq4,dq5,dq6,d_angle, finger_angle]
        """
        if len(action) < 6 or len(action) > 8:
            raise ValueError('tcp action should have either 6, 7, 8 dimensions')

        if len(action) <= 7:
            if len(action) <= 6:
                action = np.append(action, 0)

            action = np.append(action, self.finger_joint)

        if len(action) != 8:
            raise ValueError('joint action should have 6 dimensions')

        for i in range(_actionRepeat):
            self._kuka.applyAction(action, to_ctrl=kuka.JOINT)
            p.stepSimulation()
            self.timestep += 1

        return self._kuka.getJointValues()

    def get_tcp_pose(self):
        return np.array(self._kuka.getObservation()[:3])

    def get_tcp_orientation(self):
        return np.array(self._kuka.getObservation()[3:])

    def get_state(self, to_ctrl=kuka.TCP):
        if to_ctrl == kuka.TCP:
            pos = self.get_tcp_pose()
            angle = np.array(self._kuka.endEffectorAngle)
            return np.append(pos, angle)
        else:
            pos, vel, acc = self._kuka.getJointValues()
            return pos[:7]

    def control_robot(self, action):
        self.step(action=action)
        time.sleep(self._timeStep)
        self.move_camera()
        return self.stopped()

    def compute_jacobian(self):
        jact, jacr = self._kuka.calcJacobian()
        return jact, jacr

    def get_sim_time(self):
        return self.timestep * self._timeStep

    def run_controller(self, ctrl):
        while not self.started():
            time.sleep(self._timeStep)

        self.set_control_target(ctrl.to_ctrl)
        terminated = False
        while not terminated:
            terminated = self.control_robot(action=ctrl.control(self.timestep, self.get_state(ctrl.to_ctrl)))

    def run_controller_1step(self, ctrl):
        self.set_control_target(ctrl.to_ctrl)
        self.control_robot(action=ctrl.control(self.timestep, self.get_state(ctrl.to_ctrl)))

    def close_gripper(self):
        self._kuka.closeGripper()
        while self._kuka.getFingerJoint() >= 0.1:
            p.stepSimulation()
            self.timestep += 1
            time.sleep(self._timeStep)

    def open_gripper(self):
        self._kuka.openGripper()
        while self._kuka.getFingerJoint() <= 0.5:
            p.stepSimulation()
            self.timestep += 1
            time.sleep(self._timeStep)

    def set_finger_joint(self, target):
        self.finger_joint = target



if __name__ == '__main__':
    env = KukaGripperEnv(_render=True)
    env.reset_env()

    ctrl = SimpleTrajTracker(kp=.1)
    terminated = False
    while not terminated:
        terminated = env.control_robot(action=ctrl.control(env.timestep, env.get_tcp_state()))

