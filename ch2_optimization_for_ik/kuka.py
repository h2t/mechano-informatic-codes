import os, inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(os.path.dirname(currentdir))
os.sys.path.insert(0, parentdir)

import pybullet as p
import numpy as np
import copy
import math
import pybullet_data

JOINT = 0
TCP = 1

class Kuka:
    def __init__(self, urdfRootPath=pybullet_data.getDataPath(), timeStep=0.01):
        self.urdfRootPath = urdfRootPath
        self.timeStep = timeStep
        self.maxVelocity = .35
        self.maxForce = 200.
        self.fingerAForce = 2
        self.fingerBForce = 2.5
        self.fingerTipForce = 2
        self.useInverseKinematics = 1
        self.useSimulation = 1
        self.useNullSpace = 1
        self.useOrientation = 1
        self.kukaEndEffectorIndex = 6
        self.kukaGripperIndex = 7
        self.joint_ctrl_mode = p.POSITION_CONTROL
        #lower limits for null space
        self.ll = [-.967, -2, -2.96, 0.19, -2.96, -2.09, -3.05]
        #upper limits for null space
        self.ul = [.967, 2, 2.96, 2.29, 2.96, 2.09, 3.05]
        #joint ranges for null space
        self.jr = [5.8, 4, 5.8, 4, 5.8, 4, 6]
        #restposes for null space
        self.rp = [0, 0, 0, 0.5 * math.pi, 0, -math.pi * 0.5 * 0.66, 0]
        #joint damping coefficents
        self.jd = [
            0.00001, 0.00001, 0.00001, 0.00001, 0.00001, 0.00001, 0.00001, 0.00001, 0.00001, 0.00001,
            0.00001, 0.00001, 0.00001, 0.00001
        ]
        self.reset()

    def reset(self):
        self.kukaid = p.loadURDF(os.path.join(self.urdfRootPath, "kuka_iiwa/model.urdf"), useFixedBase=True)
        p.resetBasePositionAndOrientation(self.kukaid, [10, -.5000000, 0.070000],
                                          [0.000000, 0.000000, 0.000000, 1.000000])

        objects = p.loadSDF(os.path.join(self.urdfRootPath, "kuka_iiwa/kuka_with_gripper2.sdf"))
        self.kukaUid = objects[0]

        p.resetBasePositionAndOrientation(self.kukaUid, [-0.100000, 0.000000, 0.070000],
                                          [0.000000, 0.000000, 0.000000, 1.000000])

        self.jointPositions = [
            0.006418, 0.413184, -0.011401, -1.589317, 0.005379, 1.137684, -0.006539, 0.000048,
            -0.299912, 0.000000, -0.000043, 0.299960, 0.000000, -0.000200
        ]

        for jointIndex in range(p.getNumJoints(self.kukaid)):
            p.resetJointState(self.kukaid, jointIndex, self.jointPositions[jointIndex])

        self.numJoints = p.getNumJoints(self.kukaUid)
        for jointIndex in range(self.numJoints):
            p.resetJointState(self.kukaUid, jointIndex, self.jointPositions[jointIndex])
            p.setJointMotorControl2(self.kukaUid,
                                  jointIndex,
                                  p.POSITION_CONTROL,
                                  targetPosition=self.jointPositions[jointIndex],
                                  force=self.maxForce)

        self.trayUid = p.loadURDF(os.path.join(self.urdfRootPath, "tray/tray.urdf"), 0.640000,
                                  0.075000, -0.190000, 0.000000, 0.000000, 1.000000, 0.000000)
        self.endEffectorPos = [0.537, 0.0, 0.5]
        self.endEffectorAngle = 0
        self.motorNames = []
        self.motorIndices = []

        for i in range(self.numJoints):
            jointInfo = p.getJointInfo(self.kukaUid, i)
            qIndex = jointInfo[3]
            if qIndex > -1:
                self.motorNames.append(str(jointInfo[1]))
                self.motorIndices.append(i)

    def getActionDimension(self):
        if (self.useInverseKinematics):
            return len(self.motorIndices)
        return 6  #position x,y,z and roll/pitch/yaw euler angles of end effector

    def getObservationDimension(self):
        return len(self.getObservation())

    def getObservation(self):
        observation = []
        state = p.getLinkState(self.kukaUid, self.kukaEndEffectorIndex)
        pos = state[4]
        orn = state[5]
        euler = p.getEulerFromQuaternion(orn)
        observation.extend(list(pos))
        observation.extend(list(euler))
        return observation

    def getJointValues(self, id=None):
        if id is None:
            id = self.kukaUid

        joint_states = p.getJointStates(id, range(p.getNumJoints(id)))
        joint_positions = [state[0] for state in joint_states]
        joint_velocities = [state[1] for state in joint_states]
        joint_torques = [state[3] for state in joint_states]
        return joint_positions, joint_velocities, joint_torques

    def closeGripper(self):
        self.controlFinger(0)

    def openGripper(self):
        self.controlFinger(0.5)

    def controlFinger(self, fingerAngle):
        # fingers
        p.setJointMotorControl2(self.kukaUid, 8, p.POSITION_CONTROL, targetPosition=-fingerAngle, force=self.fingerAForce)
        p.setJointMotorControl2(self.kukaUid, 11, p.POSITION_CONTROL, targetPosition=fingerAngle,force=self.fingerBForce)
        p.setJointMotorControl2(self.kukaUid, 10, p.POSITION_CONTROL, targetPosition=0, force=self.fingerTipForce)
        p.setJointMotorControl2(self.kukaUid, 13, p.POSITION_CONTROL, targetPosition=0, force=self.fingerTipForce)

    def getFingerJoint(self):
        return np.abs(p.getJointState(self.kukaUid, 8)[0])

    def calcJacobian(self):
        result = p.getLinkState(self.kukaid, self.kukaEndEffectorIndex)
        pos, vel, acc = self.getJointValues(self.kukaid)
        zero_vec = [0.0] * len(pos)
        j_t, j_r = p.calculateJacobian(self.kukaid, self.kukaEndEffectorIndex, result[2], pos, zero_vec, zero_vec)
        return j_t, j_r

    def applyAction(self, motorCommands, to_ctrl=TCP):
        fingerAngle = motorCommands[-1]
        self.controlFinger(fingerAngle)
        da = motorCommands[-2]
        self.endEffectorAngle = self.endEffectorAngle + da
        p.setJointMotorControl2(self.kukaUid, 7, p.POSITION_CONTROL,targetPosition=self.endEffectorAngle, force=self.maxForce)
        if to_ctrl == TCP:
            dx = motorCommands[0]
            dy = motorCommands[1]
            dz = motorCommands[2]

            state = p.getLinkState(self.kukaUid, self.kukaEndEffectorIndex)
            actualEndEffectorPos = state[4]

            self.endEffectorPos[0] = actualEndEffectorPos[0] + dx
            if self.endEffectorPos[0] > 0.65:
                self.endEffectorPos[0] = 0.65
            if self.endEffectorPos[0] < 0.50:
                self.endEffectorPos[0] = 0.50
            self.endEffectorPos[1] = actualEndEffectorPos[1] + dy
            if self.endEffectorPos[1] < -0.17:
                self.endEffectorPos[1] = -0.17
            if self.endEffectorPos[1] > 0.22:
                self.endEffectorPos[1] = 0.22

            self.endEffectorPos[2] = actualEndEffectorPos[2] + dz
            pos = self.endEffectorPos
            orn = p.getQuaternionFromEuler([0, -math.pi, 0])
            if (self.useNullSpace == 1):
                if (self.useOrientation == 1):
                    jointPoses = p.calculateInverseKinematics(self.kukaUid, self.kukaEndEffectorIndex, pos,
                                                    orn, self.ll, self.ul, self.jr, self.rp)
                else:
                    jointPoses = p.calculateInverseKinematics(self.kukaUid,
                                                            self.kukaEndEffectorIndex,
                                                            pos,
                                                            lowerLimits=self.ll,
                                                            upperLimits=self.ul,
                                                            jointRanges=self.jr,
                                                            restPoses=self.rp)
            else:
                if (self.useOrientation == 1):
                    jointPoses = p.calculateInverseKinematics(self.kukaUid,
                                                              self.kukaEndEffectorIndex,
                                                              pos,
                                                              orn,
                                                              jointDamping=self.jd)
                else:
                    jointPoses = p.calculateInverseKinematics(self.kukaUid, self.kukaEndEffectorIndex, pos)


            if self.useSimulation:
                for i in range(self.kukaEndEffectorIndex):
                    p.setJointMotorControl2(bodyUniqueId=self.kukaUid,
                                            jointIndex=i,
                                            controlMode=p.POSITION_CONTROL,
                                            targetPosition=jointPoses[i],
                                            targetVelocity=0,
                                            force=self.maxForce,
                                            maxVelocity=self.maxVelocity,
                                            positionGain=1,
                                            velocityGain=1)
        else:
            for action in range(len(motorCommands)-2):
                motor = self.motorIndices[action]
                joint_pos = p.getJointState(self.kukaUid, motor)[0] + motorCommands[action]
                p.setJointMotorControl2(self.kukaUid,
                                        motor,
                                        p.POSITION_CONTROL,
                                        targetPosition=joint_pos,
                                        force=self.maxForce)



        for jointIndex in range(p.getNumJoints(self.kukaid)):
            joint_pos = p.getJointState(self.kukaUid, jointIndex)[0]
            p.resetJointState(self.kukaid, jointIndex, joint_pos)
